package com.epam.nadei18;

public class App {
    public static void main(String[] args) {

        BoardGame boardGame = new BoardGame();

        Chess chess = new Chess();
        chess.selectOpponent();

        boardGame.setGameStrategy(chess);

        boardGame.registerObserver(new ChessBoard(boardGame));
        boardGame.playGame();

    }
}
