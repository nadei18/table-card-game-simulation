package com.epam.nadei18;

public interface BoardGameStrategy {
    public void play();

    public void selectOpponent();
}
