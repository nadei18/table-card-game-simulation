package com.epam.nadei18;

import java.util.ArrayList;

public class BoardGame implements Observable {
    BoardGameStrategy game;
    ArrayList<Observer> observers = new ArrayList<>();

    public void playGame() {
        game.play();
    }

    public void setGameStrategy(BoardGameStrategy strategy) {
        game = strategy;
    }

    @Override
    public void registerObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(Observer o) {
        observers.remove(o);
    }

    @Override
    public void notifyObservers() {
        for (Observer o : observers) {
            o.update();
        }
    }
}
